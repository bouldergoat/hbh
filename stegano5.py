from PIL import Image
import binascii

img = Image.open('stegano5.png')
data = img.tobytes()
print(data)
binary = ''
for bit in data:
    if bit == 255:
        binary = binary + '0'
    else:
        binary = binary + '1'
print(binary)
print(len(binary))
