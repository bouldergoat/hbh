import urllib.request
from itertools import product


def getResponseCode(url):
    conn = urllib.request.urlopen(url)
    return conn.getcode()


url_base = "https://www.hellboundhackers.org/challenges/real2/backups/"

for d, h in product(range(1, 32), range(100, 2401, 100)):
    d = str(d).zfill(2)
    h = str(h).zfill(4)
    url_append = "backup_2004-09-" + d + "_" + h + ".sql"
    url_to_check = url_base + url_append
    try:
        res = getResponseCode(url_to_check)
        print(url_to_check)
        break
    except:
        print("no luck", d, h)

